import * as pc from "playcanvas";

export function setHardwareInstancingModel(model, app) {
    model.meshInstances.forEach((meshInstance) => {
        var material = meshInstance.material;
        material.onUpdateShader = function (options) {
            options.useInstancing = true;
            return options;
        };
        material.update();

        if (app.graphicsDevice.supportsInstancing) {
            const   instanceCount = 1,
                    matrices      = meshInstance.node.getWorldTransform().data;

            const vertexBuffer = new pc.VertexBuffer(
                app.graphicsDevice,
                pc.VertexFormat.defaultInstancingFormat,
                instanceCount,
                pc.BUFFER_STATIC,
                matrices
            );

            meshInstance.setInstancing(vertexBuffer);
        }

    });
}
