import * as pc from "playcanvas";
import App from "../app";
import AssetManager, { AssetManagerEvent } from "../asset_manager";
import Bullet from "../bullet";
import FirstPersonMovement from "./first_person_movement";
import PlayerEvent from "./player_event";

export const PersonEvent = Object.freeze({
    Collided: "personevent:collided",
    Dead: "personevent:dead",
});

export default class Player extends pc.Entity {
    constructor(app) {
        super("Player");
        this.app = app;
        this.health = 100;
        this.blendTime = 0.02;
        this.lastCollidedTime = 0;
        this.lasCollidedEntity = null;
        this._init();
    }

    _init() {
        this.addComponent("collision", {
            type: "capsule",
            radius: 0.5,
            height: 2,
        });
        this.addComponent("rigidbody", {
            angularDamping: 0,
            angularFactor: pc.Vec3.ZERO,
            friction: 1,
            linearDamping: 0,
            linearFactor: pc.Vec3.ONE,
            mass: 80,
            restitution: 0,
            type: "dynamic",
        });

        this.container = new pc.Entity("container");
        this.addChild(this.container);

        this.weapon = new pc.Entity("Hand Ak47");
        this.weapon.setPosition(0.2, 0.45, -0.2);
        this.weapon.setEulerAngles(0, 180, -10);

        this.app.assets.loadFromUrl(
            AssetManager.getAssetPath("Hand Ak47"),
            "model",
            (err, asset) => {
                this.weapon.addComponent("model", {
                    type: "asset",
                    asset: asset,
                    layers: [this.app.fpsViewLayer.id],
                    castShadows: false,
                });
                this.container.addChild(this.weapon);
                this._initFireParticle();
                this.fire(AssetManagerEvent.AssetLoad, App.AssetLoaded + 1);
            }
        );

        this.skin = new pc.Entity("Skin");
        this.app.assets.loadFromUrl(
            AssetManager.getAssetPath("Player"),
            "model",
            (err, asset) => {
                this.skin.addComponent("model", {
                    type: "asset",
                    asset: asset,
                    layers: [this.app.mapViewLayer.id],
                });
                this.fire(AssetManagerEvent.AssetLoad, App.AssetLoaded + 1);
            }
        );
        this.addChild(this.skin);

        this._setupCamera();

        this.barrel = new pc.Entity("Barrel");

        this.container.addChild(this.barrel);
        this.barrel.setLocalScale(0.05, 0.05, 0.05);
        this.barrel.setLocalPosition(0.2, 0.58, -2.5);
        this.bullet = new Bullet();

        new FirstPersonMovement(this.app, this);
        new PlayerEvent(this.app, this);

        this.collision.on("contact", this.onCollision, this);
    }

    onCollision(e) {
        if (e.other.name === "monster") {
            const now = Date.now();
            if (
                this.lasCollidedEntity !== e.other ||
                now - this.lastCollidedTime > 500
            ) {
                this.health -= 10;
                this.fire(PersonEvent.Collided, this.health);
                this.lasCollidedEntity = e.other;
                this.lastCollidedTime = now;
            }
        }
    }

    shoot(to, timeToMove) {
        this.bullet.shoot(
            this.app,
            this.barrel.getPosition(),
            to,
            timeToMove,
            this.container.getEulerAngles()
        );
    }

    _initFireParticle() {
        this.fireParticle = new pc.Entity("Fire Particle");
        this.fireParticle.addComponent("particlesystem", {
            enabled: true,
            autoPlay: true,
            loop: false,
            numParticles: 1,
            lifetime: 0.1,
            blendType: pc.BLEND_ADDITIVE,
            animTilesX: 3,
            animTilesY: 2,
            animNumFrames: 6,
            animSpeed: 1,
            animLoop: true,
            colorMapAsset: this.app.assets.find("Hit Sprite Low"),
            colorGraph: {
                type: 1,
                keys: [
                    [0.0125, 0.9921, 0.6218, 1, 0.975, 1],
                    [0.0125, 0.1882, 0.6218, 0.4705, 0.975, 0.9254],
                    [0.0125, 0.07058, 0.6218, 0.247, 0.975, 0.31764],
                ],
                betweenCurves: false,
            },
            layers: [this.app.fpsViewLayer.id],

            scaleGraph: {
                type: pc.CURVE_SMOOTHSTEP,
                keys: [0, 0.1],
                betweenCurves: false,
            },
            scaleGraph2: {
                type: 1,
                keys: [0, 0.1],
            },
        });

        this.fireParticle.particlesystem.localSpace = true;

        this.weapon.findByName("bolt").addChild(this.fireParticle);
        this.fireParticle.setLocalPosition(-1, -1, 60);
    }

    _setupCamera() {
        this.camera = new pc.Entity("camera");
        this.camera.addComponent("camera", {
            clearColorBuffer: true,
            clearColor: new pc.Color(0.4, 0.45, 0.5),
            farClip: 100,
            fov: 45,
            nearClip: 0.1,
            layers: [
                this.app.fpsViewLayer.id,
                this.app.uiLayer.id,
                this.app.worldLayer.id,
                this.app.skyboxLayer.id,
            ],
        });
        this.camera.setPosition(0, 0.7, 0);
        this.container.addChild(this.camera);
    }

    playAnimation(animation) {
        if (animation === "FPS Fire") {
            if (!this.weapon.animation) {
                this.weapon.addComponent("animation", {
                    loop: false,
                });
            }
            if (!this.weapon.animation.getAnimation(animation)) {
                let tmp = this.weapon.animation.assets;
                tmp.push(this.app.assets.find(animation));
                this.weapon.animation.assets = tmp;
            }
            this.weapon.animation.play(animation, this.blendTime);
        } else {
            if (!this.skin.animation) {
                this.skin.addComponent("animation", {
                    loop: true,
                });
            }
            if (!this.skin.animation.getAnimation(animation)) {
                let tmp = this.skin.animation.assets;
                tmp.push(this.app.assets.find(animation));
                this.skin.animation.assets = tmp;
            }
            this.skin.animation.play(animation, this.blendTime);
        }
    }

    playSound(sound) {
        if (sound === "AK47 Fire Sound") {
            if (!this.weapon.sound) {
                this.weapon.addComponent("sound", {
                    positional: false,
                });
            }
            if (!this.weapon.sound.slot(sound)) {
                this.weapon.sound.addSlot(sound, {
                    loop: false,
                    asset: this.app.assets.find(sound),
                });
            }
            this.weapon.sound.play(sound);
        } else {
            if (!this.skin.sound) {
                this.skin.addComponent("sound");
            }
            if (!this.skin.sound.slot(sound)) {
                this.skin.sound.addSlot(sound, {
                    loop: false,
                });
            }
            this.skin.sound.play(sound);
        }
    }
}
