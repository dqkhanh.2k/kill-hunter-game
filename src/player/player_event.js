import * as pc from "playcanvas";
import Bullet from "../bullet";

export default class PlayerEvent {
    constructor(app, player) {
        this.app = app;
        this.player = player;
        this.keyboard = this.app.keyboard;
        this.mouse = this.app.mouse;
        this.animation = "Idle";

        this.keyboard.on(pc.EVENT_KEYDOWN, this.keyChange, this);
        this.keyboard.on(pc.EVENT_KEYUP, this.keyChange, this);

        this.mouse.on(pc.EVENT_MOUSEDOWN, this.mouseDown, this);
        this.mouse.on(pc.EVENT_MOUSEUP, this.mouseUp, this);

        this.shotting = false;
        this.shootSpeed = 60 / 600; // 600 time per minute
        this.lastShootTime = 0;
        this.force = new pc.Vec3();
        this.app.on("update", this.update, this);
        this.power = 30;
        this.bulletSpeed = 500; // m/s
        
    }

    update(dt) {
        if (this.shotting) {
            this.lastShootTime += dt;
            if (this.lastShootTime >= this.shootSpeed) {
                this.player.playAnimation("FPS Fire");
                this.player.playSound("AK47 Fire Sound");
                this.shoot();

                this.player.fireParticle.particlesystem.reset();
                this.player.fireParticle.particlesystem.play();
                this.lastShootTime = 0;
            }
        }
    }

    shoot() {
        var screenCenterX = this.app.graphicsDevice.width / 2 / window.devicePixelRatio;
        var screenCenterY = this.app.graphicsDevice.height / 2 / window.devicePixelRatio;
        var to = this.player.camera.camera.screenToWorld(
            screenCenterX,
            screenCenterY,
            this.player.camera.camera.farClip
        );
        var from = this.player.camera.camera.screenToWorld(
            screenCenterX,
            screenCenterY,
            this.player.camera.camera.nearClip
        );

        const result = this.app.systems.rigidbody.raycastFirst(from, to);
        if(result){
            let distance = from.distance(result.point)
            let timeToMove = distance/this.bulletSpeed * 1000; // in millisecond an scale 10 time

            this.player.shoot(to, timeToMove);
            setTimeout(() => {
                if(result.entity.name === "monster"){
                    result.entity.beShooted()
                }
            }, timeToMove)
            
        }
    }

    // check movement direction to set animation for skin
    keyChange() {
        let tempAnimation = this.animation;

        this.checkActions();

        if (tempAnimation !== this.animation) {
            this.player.playAnimation(this.animation);
        }
    }

    mouseDown(e) {
        this.shotting = true;
    }

    mouseUp(e) {
        this.shotting = false;
    }

    checkActions() {
        let w = this.keyboard.isPressed(pc.KEY_W);
        let a = this.keyboard.isPressed(pc.KEY_A);
        let s = this.keyboard.isPressed(pc.KEY_S);
        let d = this.keyboard.isPressed(pc.KEY_D);

        if (w && !s) {
            if (a && !d) {
                this.animation = "Run Forward Left";
            } else if (d && !a) {
                this.animation = "Run Forward Right";
            } else {
                this.animation = "Run Forward";
            }
        } else if (s && !w) {
            if (a && !d) {
                this.animation = "Run Backward Left";
            } else if (d && !a) {
                this.animation = "Run Backward Right";
            } else {
                this.animation = "Run Backward";
            }
        } else if (a && !d) {
            this.animation = "Run Left";
        } else if (d && !a) {
            this.animation = "Run Right";
        } else {
            this.animation = "Idle";
        }
    }

    _createBullet() {}
}
