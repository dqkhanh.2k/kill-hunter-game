import * as pc from "playcanvas";

export default class FirstPersonMovement {
    constructor(app, entity) {
        this.power = 1;
        this.lookSpeed = 0.25;

        this.force = new pc.Vec3();
        this.eulers = new pc.Vec3();
        this.app = app;
        this.entity = entity;
        this.playerContainer = entity.findByName("container");
        this.camera = entity.findByName("camera");
        this.weapon = entity.findByName("Hand Ak47");
        this.skin = entity.findByName("Skin");
        this.onGround = true;
        this.jumping = false;

        app.mouse.on("mousemove", this._onMouseMove, this);

        app.mouse.on(
            "mousedown",
            function () {
                app.mouse.enablePointerLock();
            },
            this
        );

        app.on("update", this.update, this);

        this.entity.rigidbody.on("collisionstart", (e) => {
            if (e.other.name === "world") {
                this.onGround = true;
                this.jumping = false;
            }
        });

        this.entity.rigidbody.on("collisionend", (e) => {
            if (e.name === "world") {
                this.onGround = false;
                this.jumping = true;
            }
        });
    }

    update(dt) {
        var force = this.force;
        var app = this.app;

        // Get camera directions to determine movement directions
        var forward = this.playerContainer.forward;
        var right = this.playerContainer.right;

        // movement
        var x = 0;
        var z = 0;

        if(app.keyboard.isPressed(pc.KEY_SHIFT)){
            this.power = 10;
        }
        else{
            this.power = 5;
        }

        // Use W-A-S-D keys to move player
        // Check for key presses
        if (app.keyboard.isPressed(pc.KEY_A)) {
            x -= right.x;
            z -= right.z;
        }

        if (app.keyboard.isPressed(pc.KEY_D)) {
            x += right.x;
            z += right.z;
        }

        if (app.keyboard.isPressed(pc.KEY_W)) {
            x += forward.x;
            z += forward.z;
        }

        if (app.keyboard.isPressed(pc.KEY_S)) {
            x -= forward.x;
            z -= forward.z;
        }

        this.playerContainer.setEulerAngles(this.eulers.y, this.eulers.x, 0);
        this.skin.setEulerAngles(0, this.eulers.x + 180, 0);

        if (x !== 0 && z !== 0) {
            force.set(x, 0, z).normalize().scale(this.power);
            if (this.onGround && !this.jumping) {
                this.entity.rigidbody.linearVelocity = force;
            } else {
                this.entity.rigidbody.applyForce(force.scale(this.power*7));
            }
        }

        if (
            this.app.keyboard.isPressed(pc.KEY_SPACE) &&
            this.onGround &&
            !this.jumping
        ) {
            this.force.set(0, 10, 0).normalize().scale(this.power);
            this.entity.rigidbody.linearVelocity = this.force;
            this.jumping = true;
        }
    }

    _onMouseMove(e) {
        if (pc.Mouse.isPointerLocked() || e.buttons[0]) {
            this.eulers.x -= this.lookSpeed * e.dx;
            this.eulers.y -= this.lookSpeed * e.dy;
        }
    }
}
