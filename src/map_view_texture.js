import * as pc from "playcanvas";

export default class MapViewTexture extends pc.Texture {
    constructor(app, width, height, flipY = true) {
        super(app.graphicsDevice, {
            width: width,
            height: height,
            format: pc.PIXELFORMAT_R8_G8_B8,
            mipmaps: true,
            minFilter: pc.FILTER_LINEAR,
            magFilter: pc.FILTER_LINEAR,
            addressU: pc.ADDRESS_CLAMP_TO_EDGE,
            addressV: pc.ADDRESS_CLAMP_TO_EDGE,
        });

        this.renderTarget = new pc.RenderTarget({
            colorBuffer: this,
            depth: true,
            flipY: flipY,
        });

        this.camera = new pc.Entity("Map View Camera");

        this.camera.addComponent("camera", {
            layers: [app.mapViewLayer.id, app.worldLayer.id, app.skyboxLayer.id],
            priority: -1,
            renderTarget: this.renderTarget,
            clearColorBuffer: true,
            clearColor: new pc.Color(0.4, 0.45, 0.5),
            farClip: 1000,
            fov: 45,
            nearClip: 0.1,
        });

        this.camera.setEulerAngles(-90, 0, 0)


        const player = app.root.findByName("Player");
        app.on("update", () => {
            const pos = player.getPosition();
            this.camera.setPosition(pos.x, pos.y+10, pos.z);
            
        })
        app.root.addChild(this.camera);
    }
}
