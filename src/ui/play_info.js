import * as pc from "playcanvas";
import MapViewTexture from "../map_view_texture";

export default class PlayInfoScreen extends pc.Entity {
    constructor(app) {
        super("score screen");
        this.app = app;
        this.addComponent("screen", {
            resolution: [1280, 720],
            referenceResolution: [1080, 1920],
            screenSpace: true,
        });
        this._init();
    }

    _init() {
        this.mapView = new pc.Entity("Map View");
        const mapViewTexture = new MapViewTexture(this.app, 240, 128)
        this.mapView.addComponent("element", {
            type: pc.ELEMENTTYPE_IMAGE,
            width: 240,
            height: 128,
            anchor: pc.Vec4.ONE,
            pivot: pc.Vec2.ONE,
            texture: mapViewTexture
        })
        this.addChild(this.mapView)

        const aimCursor = new pc.Entity("Aim Cursor");
        aimCursor.addComponent("element", {
            type: pc.ELEMENTTYPE_IMAGE,
            width: 50,
            height: 50,
            anchor: new pc.Vec4(0.5, 0.5, 0.5, 0.5),
            pivot: new pc.Vec2(0.5, 0.5),
            textureAsset: this.app.assets.find("Aim Cursor"),
        });
        
        this.addChild(aimCursor);

        this.txtHealth = new pc.Entity("Text Health");
        this.txtHealth.addComponent("element", {
            type: pc.ELEMENTTYPE_TEXT,
            anchor: pc.Vec4.ZERO,
            pivot: pc.Vec2.ZERO,
            text: "100",
            fontSize: 80,
            fontAsset: this.app.assets.find("font")
        })

        this.addChild(this.txtHealth)

    }

    
}
