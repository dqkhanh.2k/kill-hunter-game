import { Tween, autoPlay } from "es6-tween";
import * as pc from "playcanvas";

export default class Bullet extends pc.Entity {
    constructor() {
        super("bullet");
        this.addComponent("model", {
            type: "sphere",
        });

        
        this.setLocalScale(0.5, 0.05, 0.1)
        this.setLocalEulerAngles(0, 90, 0)
        this.setLocalPosition(0, 1, 0);

        const material = new pc.StandardMaterial();
        material.diffuse = new pc.Color(1, 0.6, 0);
        material.update();
        this.model.meshInstances[0].material = material;
    }

    clone(){
        let entity = super.clone()
        entity.onCollision = this.onCollision;
        return entity;
    }

    shoot(app, from, to, duration, eulerAngle){
        autoPlay(true);

        eulerAngle.y += 90;

        const bullet = this.clone();
        bullet.setEulerAngles(eulerAngle);
        bullet.setPosition(from);
        app.root.addChild(bullet);

        const tween = new Tween(from);
        tween.to(to, duration)
                .on("update", pos => {
                    bullet.setPosition(pos);
                })
                .on("complete", bullet.onCollision.bind(bullet))
                .start()
    }

    onCollision(){
        this.destroy()
    }
}
