import * as pc from "playcanvas";
import AssetManager, { AssetManagerEvent } from "./asset_manager.js";
import Monster from "./moster/monster.js";
import { MonsterManager } from "./moster/monsterManager.js";
import Player, { PersonEvent } from "./player/player.js";
import PlayInfoScreen from "./ui/play_info.js";
import World from "./world.js";
import {update} from '@tweenjs/tween.js';

export const AppEvent = Object.freeze({
    AssetProgress: "appevent:assetprogress",
    AssetLoaded: "appevent:assetloaded",
});

export default class App extends pc.Application {
    static TotalAsset = 5;
    static AssetLoaded = 0;
    constructor(canvas, useKeyboard = true, useMouse = true) {
        const config = {
            keyboard: useKeyboard ? new pc.Keyboard(window) : null,
            mouse: useMouse ? new pc.Mouse(canvas) : null,
            elementInput: new pc.ElementInput(canvas),
        };
        super(canvas, config);

        this.assetManager = new AssetManager(this);
        this.assetManager.loadAsset();
        this.preload(() => {
            this.start();
            this.handleLoadAsset();
            this._init();
            this.root.enabled = false;
        });
    }

    _init() {
        this.systems.rigidbody.gravity.set(0, -9.81, 0);
        this.setCanvasFillMode(pc.FILLMODE_FILL_WINDOW);
        this.setCanvasResolution(pc.RESOLUTION_AUTO);
        window.addEventListener("resize", () => this.resizeCanvas());
        this.scene.ambientLight = new pc.Color(0.4, 0.4, 0.4);
        this._initLayer();

        this.player = new Player(this);
        this.player.setPosition(0, 3, 0);
        this.root.addChild(this.player);
        this.player.on(AssetManagerEvent.AssetLoad, this.handleLoadAsset, this);

        this.world = new World(this);
        this.root.addChild(this.world);
        this.world.on(AssetManagerEvent.AssetLoad, this.handleLoadAsset, this);

        this.monsterManager = new MonsterManager(this);
        this.on("update", this.monsterManager.update, this.monsterManager);
        this.monsterManager.on(AssetManagerEvent.AssetLoad, this.handleLoadAsset, this);

        this.infoScreen = new PlayInfoScreen(this);
        this.player.on(PersonEvent.Collided, (health) => {
            this.infoScreen.txtHealth.element.text = health;
        })
        this.root.addChild(this.infoScreen);
    }

    _initLayer() {
        this.uiLayer = this.scene.layers.getLayerByName("UI");
        this.worldLayer = this.scene.layers.getLayerByName("World");
        this.skyboxLayer = this.scene.layers.getLayerByName("Skybox");
        this.mapViewLayer = new pc.Layer({
            name: "Map View",
        });

        this.fpsViewLayer = new pc.Layer({
            name: "FPS View",
        });

        this.scene.layers.push(this.mapViewLayer);
        this.scene.layers.push(this.fpsViewLayer);
    }

    handleLoadAsset() {
        App.AssetLoaded += 1;

        let percent = App.AssetLoaded / App.TotalAsset;
        this.fire(AppEvent.AssetProgress, percent);
        if (App.AssetLoaded === App.TotalAsset) {
            this.fire(AppEvent.AssetLoaded);
        }
    }
}
