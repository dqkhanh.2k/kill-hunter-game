export function wasmSupported() {
    try {
        if (
            typeof WebAssembly === "object" &&
            typeof WebAssembly.instantiate === "function"
        ) {
            const module = new WebAssembly.Module(
                Uint8Array.of(0x0, 0x61, 0x73, 0x6d, 0x01, 0x00, 0x00, 0x00)
            );
            if (module instanceof WebAssembly.Module)
                return (
                    new WebAssembly.Instance(module) instanceof
                    WebAssembly.Instance
                );
        }
    } catch (e) {}
    return false;
}

export function loadScriptAsync(url, doneCallback) {
    const tag = document.createElement("script");
    tag.onload = function () {
        doneCallback();
    };
    tag.onerror = function () {
        throw new Error(`failed to load ${url}`);
    };
    tag.async = true;
    tag.src = url;
    document.head.appendChild(tag);
}

export function loadWasmModuleAsync(moduleName, jsUrl, binaryUrl, doneCallback) {
    loadScriptAsync(jsUrl, () => {
        const lib = window[moduleName];
        window[`${moduleName}Lib`] = lib;
        lib({
            locateFile() {
                return binaryUrl;
            },
        }).then((instance) => {
            window[moduleName] = instance;
            doneCallback();
        });
    });
}

export function loadAmmo(){
    return new Promise((resolve) => {
        if (wasmSupported()) {
            loadWasmModuleAsync(
                "Ammo",
                "vendors/ammo.wasm.js",
                "vendors/ammo.wasm.wasm",
                () => {
                    resolve();
                }
            );
        } else {
            loadWasmModuleAsync("Ammo", "./vendors/ammo.js", "", () => {
                resolve();
            });
        }
    });

}
    