import { Asset, Color, StandardMaterial } from "playcanvas";
import assetJSON from "./asset.json";

export const AssetManagerEvent = Object.freeze({
    AssetLoad: "assetmanagerevent:assetload"
})

export default class AssetManager {
    constructor(app) {
        this.app = app;
    }

    loadAsset() {
        assetJSON.forEach((raw) => {
            if (raw.type === "model") return;
            let path = `assets/${raw.path}`;
            let asset = new Asset(raw.name, raw.type, {
                url: path,
            });
            asset.preload = true;
            this.app.assets.add(asset);
            this.app.assets.load(asset);
        });
    }

    static getAssetPath(name) {
        for (let raw of assetJSON) {
            if (raw.name === name) {
                let path = `assets/${raw.path}`;
                return path;
            }
        }
    }

    static loadMaterial(model, name, app) {
        materialJSON.forEach((raw) => {
            if (raw.model === name) {
                raw.material.forEach((textureName, index) => {
                    if (model.meshInstances[index])
                        model.meshInstances[index].material =
                            AssetManager._createMaterialTexture(
                                textureName,
                                app
                            );
                });
                return;
            }
        });
    }

    static _createMaterialTexture(name, app) {
        let material = new StandardMaterial();

        let diffuseTexture = app.assets.find(`${name}_diffuse.png`);
        if (diffuseTexture) {
            material.diffuse = new Color(0.8, 0.8, 0.8);
            material.diffuseMap = diffuseTexture.resource;
        }

        let specularTexture = app.assets.find(`${name}_specular.png`);
        if (specularTexture !== null && specularTexture !== undefined) {
            material.specular = new Color(0.5, 0.5, 0.5);
            material.specularMap = specularTexture.resource;
        }

        let normalTexture = app.assets.find(`${name}_normal.png`);
        if (normalTexture) material.normalMap = normalTexture.resource;

        material.update();
        return material;
    }
}
