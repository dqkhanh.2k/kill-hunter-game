import App, { AppEvent } from "./app";
const container = document.querySelector(".container");
const progress = document.getElementById("progress-value");
const canvas = document.getElementById("canvas");
import { loadAmmo } from "./vendors/wasm-loader";

window.onload = () => {
    loadAmmo().then(() => {
        run();
    });
};

function run() {
    const app = new App(canvas);
    app.on(AppEvent.AssetProgress, setProgress);
    app.on(AppEvent.AssetLoaded, function () {
        container.style.display = "none";
        canvas.style.display = "inline";
        app.root.enabled = true;
    });
}

function setProgress(value) {
    progress.style.width = value * 100 + "%";
}
