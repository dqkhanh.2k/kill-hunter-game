import * as pc from "playcanvas";
import App from "./app";
import AssetManager, { AssetManagerEvent } from "./asset_manager";
import { setHardwareInstancingModel } from "./lib/utils";

export default class World extends pc.Entity {
    constructor(app) {
        super("world");
        this.app = app;

        const light = new pc.Entity();
        light.addComponent("light", {
            type: pc.LIGHTTYPE_DIRECTIONAL,
            layers: [
                this.app.worldLayer.id,
                this.app.fpsViewLayer.id,
                this.app.mapViewLayer.id,
            ],
            castShadows: true,
            normalOffsetBias: 0.05,
            shadowBias: 0.2,
            shadowDistance: 40,
            shadowResolution: 2048,
        });
        this.addChild(light);
        light.setLocalEulerAngles(45, 30, 0);

        this.app.assets.loadFromUrl(
            AssetManager.getAssetPath("Map"),
            "model",
            (err, asset) => {
                this.addComponent("model", {
                    type: "asset",
                    asset: asset,
                });

                this.addComponent("collision", {
                    type: "mesh",
                    asset: asset,
                });
                this.addComponent("rigidbody", {
                    type: "static",
                    restitution: 0.5,
                    friction: 0.9,
                });
                this.fire(AssetManagerEvent.AssetLoad);
            }
        );

        const material = new pc.StandardMaterial();
        material.onUpdateShader = function (options) {
            options.useInstancing = true;
            return options;
        };
        material.diffuse = new pc.Color(0.1, 1, 0);
        material.diffuseTint = true;
        material.update();

        const box = new pc.Entity("box");

        box.addComponent("render", {
            type: "box",
            material: material
        });

        // this.addChild(box)

        if (app.graphicsDevice.supportsInstancing) {
            const instanceCount = 10000;

            const matrices = new Float32Array(instanceCount * 16);
            let matrixIndex = 0;

            const radius = 5;
            const pos = new pc.Vec3();
            const rot = new pc.Quat();
            const scl = new pc.Vec3();
            const matrix = new pc.Mat4();

            for (let i = 0; i < instanceCount; i++) {
                pos.set(
                    Math.random() * radius - radius * 0.5,
                    0.2,
                    Math.random() * radius - radius * 0.5
                );
                scl.set(0.05, 0.1 + Math.random() * 0.5, 0.05);
                rot.setFromEulerAngles(0, 90, 0);
                matrix.setTRS(pos, rot, scl);

                for (let m = 0; m < 16; m++)
                    matrices[matrixIndex++] = matrix.data[m];
            }

            const vertexBuffer = new pc.VertexBuffer(
                app.graphicsDevice,
                pc.VertexFormat.defaultInstancingFormat,
                instanceCount,
                pc.BUFFER_STATIC,
                matrices
            );

            const boxMeshInst = box.render.meshInstances[0];
            boxMeshInst.setInstancing(vertexBuffer);
        }
    }
}
