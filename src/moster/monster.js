import * as pc from "playcanvas";
import App from "../app";
import AssetManager, { AssetManagerEvent } from "../asset_manager";
import { setHardwareInstancingModel } from "../lib/utils";

export default class Monster extends pc.Entity {
    constructor(app) {
        super("monster");
        this.app = app;
        this.speed = 5;
        this.blendTime = 0.02;
        this.health = 1;
        this._init();
        this.force = new pc.Vec3();
        this.onGround = true;
    }
    _init() {
        this.addComponent("collision", {
            type: "capsule",
            radius: 1,
            height: 1,
        });
        this.addComponent("rigidbody", {
            angularDamping: 0,
            angularFactor: pc.Vec3.ZERO,
            friction: 1,
            linearDamping: 0.2,
            linearFactor: pc.Vec3.ONE,
            mass: 80,
            restitution: 0.5,
            type: "dynamic",
        });

        this.skin = new pc.Entity("skin");
        this.addChild(this.skin);
        this.skin.setLocalPosition(0, -1, 0);

        this.app.assets.loadFromUrl(
            AssetManager.getAssetPath("Mutant"),
            "model",
            (err, asset) => {
                this.skin.addComponent("model", {
                    type: "asset",
                    asset: asset,
                    layers: [
                        this.app.mapViewLayer.id,
                        this.app.fpsViewLayer.id,
                    ],
                });
                this.fire(AssetManagerEvent.AssetLoad, App.AssetLoaded + 1);
                setTimeout(() => {
                    this.skin.animation.play(this.nowPlayingAnimation);
                }, 1000);
            }
        );
        this.listAnimation = [];
        this.app.assets._assets.forEach((asset) => {
            if (asset.name.includes("Mutant") && asset.type === "animation") {
                this.listAnimation.push(asset);
            }
        });
        this.skin.addComponent("animation", {
            loop: true,
        });
        this.skin.animation.assets = this.listAnimation;

        this.nowPlayingAnimation = "Mutant Run";

        this.skin.animation.play("Mutant Idle");

        this._initHealthBar();

        this.collision.on("contact", this.onCollision, this);
        this.player = this.app.root.findByName("Player");

        this.rigidbody.on("collisionstart", (e) => {
            if (e.other.name === "world") {
                this.onGround = true;
            }
        });

        this.rigidbody.on("collisionend", (e) => {
            if (e.name === "world") {
                this.onGround = false;
            }
        });
    }

    beShooted(){
        this.health -= 0.1;
            this.healthBar.setLocalScale(this.health, 1.1, 1.1);
            this.healthBar.setLocalPosition(this.health - 1, 0, 0);

            if (this.health <= 0.3) {
                this.removeComponent("rigidbody");
                this.removeComponent("collision")
                this.dying();
                this.dead = true;
                this.healthBarBound.enabled = false;
            }
    }

    onCollision(e) {
        if (e.other.name === "bullet") {
            
        }
    }

    dying() {
        this.skin.animation.loop = false;
        this.skin.animation.play("Mutant Dying");
    }

    _initHealthBar() {
        this.healthBarBound = new pc.Entity("Health Bar Bound");
        this.healthBarBound.addComponent("model", {
            type: "box",
        });
        this.healthBarBound.setLocalScale(0.5, 0.1, 0.1);
        this.skin.addChild(this.healthBarBound);
        this.healthBarBound.setLocalPosition(0, 1.9, 0);

        this.healthBar = new pc.Entity("Health Bar ");
        this.healthBar.addComponent("model", {
            type: "box",
        });
        let redMaterial = new pc.StandardMaterial();
        redMaterial.diffuse = pc.Color.RED;
        redMaterial.update();
        this.healthBar.model.meshInstances[0].material = redMaterial;
        this.healthBar.setLocalScale(this.health, 1.1, 1.1);
        this.healthBar.setLocalPosition(this.health - 1, 0, 0);
        this.healthBarBound.addChild(this.healthBar);
    }

    update(dt) {
        if (this.dead) return;
        let pos = this.player.getPosition();
        pos.y = 0;
        this.skin.lookAt(pos);
        let angle = this.skin.getEulerAngles();
        angle.y -= 180;
        this.skin.setEulerAngles(angle);

        this.force.copy(this.skin.forward);
        this.force.y = 0;
        this.force.normalize().mulScalar(-1).scale(this.speed);
        if (this.onGround) {
            this.rigidbody.linearVelocity = this.force;
        } else {
            this.rigidbody.applyForce(this.force.scale(this.speed*7));
        }
    }
}
