import * as pc from "playcanvas";
import { AssetManagerEvent } from "../asset_manager";
import { setHardwareInstancingModel } from "../lib/utils";
import Monster from "./monster";

export class MonsterManager extends pc.EventHandler {
    constructor(app) {
        super();
        this.noOfMonster = 5;
        this.app = app;

        this.listMonsters = [];

        var i = 1;
        this.listMonsters.push(this.createMonster(true));

        while (i < this.noOfMonster) {
            i++;
            this.listMonsters.push(this.createMonster());
        }
    }

    update(dt) {
        this.listMonsters.forEach((monster) => monster.update(dt));
    }

    createMonster(needHandleLoad = false) {
        var monster = new Monster(this.app),
            x = Math.random() * 30,
            z = Math.random() * 30;
        monster.setPosition(x, 1, z);
        this.app.root.addChild(monster);
        if (needHandleLoad) {
            monster.on(AssetManagerEvent.AssetLoad, () => {
                this.fire(AssetManagerEvent.AssetLoad);

                // setHardwareInstancingModel(
                //     monster.skin.model,
                //     this.app
                // );
            });
        }
        return monster;
    }
}
